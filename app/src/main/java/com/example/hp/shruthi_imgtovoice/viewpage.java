package com.example.hp.shruthi_imgtovoice;

import android.app.Activity;

import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.speech.tts.TextToSpeech.OnInitListener;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;

/**
 * Created by HP on 18-08-2017.
 */

public class viewpage extends Activity implements OnInitListener{
EditText edit;
    ImageButton bt;
    private TextToSpeech repeatTTS;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_page);
        edit = (EditText) findViewById(R.id.editText3);
        edit.setText(getIntent().getStringExtra("mytext"));
        bt=(ImageButton)findViewById(R.id.ibt);
        repeatTTS = new TextToSpeech(this, this);
        bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
         repeatTTS.speak(edit.getText().toString(),TextToSpeech.QUEUE_FLUSH,null);

            }
        });
    }
    public void stop(View view)
    {
       repeatTTS.stop();
    }
    @Override
    public void onInit(int arg0) {
// TODO Auto-generated method stub
    }
}
